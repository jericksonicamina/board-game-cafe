<nav class="uk-navbar-container uk-margin" uk-navbar>
    <div class="uk-navbar-left">
        <span class="uk-navbar-toggle" uk-icon="icon: grid; ratio: 1.3;"></span>
        <a class="uk-navbar-item uk-logo" href="{{ url('/') }}">Board Game Cafe</a>
        <div class="uk-navbar-item">
            @if(auth()->user())
                <a href="{{ route('login_check') }}">Dashboard</a>
            @endif
        </div>
    </div>

    <div class="uk-navbar-right">
        <ul class="uk-navbar-nav">
            <li>
                <div class="uk-navbar-item">
                    <form class="uk-search uk-search-default" onsubmit="return false">
                        <span class="uk-search-icon-flip" uk-search-icon></span>
                        <input class="uk-search-input" type="search" id="global-game-list-search" placeholder="Search Games...">
                    </form>
                    <div id="global-search-container">
                        <ul class="uk-list"></ul>
                    </div>
                </div>
            </li>
            <li>
                <a href="#">
                    <span uk-icon="icon: user"></span>
                    @if(auth()->user())
                        <span id="current-user-name">{{ auth()->user()->name }}</span>
                    @else
                        <span>Visitor</span>
                    @endif
                </a>
                <div class="uk-navbar-dropdown" uk-dropdown="mode:click">
                    <ul class="uk-nav uk-navbar-dropdown-nav" id="nav-user-actions">
                        <li class="uk-active">Actions</li>
                        @if(auth()->user())
                            <li>
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </li>
                        @else
                            <li>
                                <a href="{{ route('login') }}">Login</a>
                                <a href="{{ route('register') }}">Register</a>
                            </li>
                        @endif
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</nav>
