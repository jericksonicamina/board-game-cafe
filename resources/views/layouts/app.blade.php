<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Board Game Cafe') }}</title>

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('plugins/uikit-3.5.7/css/uikit.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/DataTables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/DataTables/Responsive/css/responsive.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/Select2/css/select2.min.css') }}">
    <link href="{{ mix('css/app.css') }}" rel="stylesheet">
    @yield('additional_css')
</head>
<body>
<div id="app">
    @include('layouts.header')
    @yield('content')
    @include('layouts.footer')
</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('plugins/uikit-3.5.7/js/uikit.min.js') }}"></script>
<script src="{{ asset('plugins/uikit-3.5.7/js/uikit-icons.min.js') }}"></script>
<script src="{{ asset('plugins/DataTables/datatables.min.js') }}"></script>
<script src="{{ asset('plugins/DataTables/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('plugins/Select2/js/select2.full.min.js') }}"></script>
@yield('additional_js')
</body>
</html>
