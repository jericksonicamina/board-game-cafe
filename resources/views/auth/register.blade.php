@extends('layouts.app')

@section('additional_css')
    <style>
        .register-container {
            display: flex;
            align-items: center;
            justify-content: center;
            padding: 40px 0;
            min-height: 72.1vh;
        }
        .register-wrapper {
            width: 500px;
        }
    </style>
@endsection

@section('content')
    <div class="uk-container register-container">
        <div class="uk-child-width-1-2@s uk-grid-match">
            <div class="register-wrapper">
                <div class="uk-card uk-card-default uk-card-hover uk-card-body">
                <h3 class="uk-card-title">{{ __('Register') }}</div>
                <form method="POST" class="uk-form-stacked" action="{{ route('register') }}">
                    @csrf
                    <div class="uk-margin">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>
                        <div class="col-md-6">
                            <input id="name" type="text" class="uk-input @error('name') uk-form-danger @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
                            @error('name')
                            <span class="uk-alert-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="uk-margin">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" class="uk-input @error('email') uk-form-danger @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                        @error('email')
                        <span class="uk-alert-danger" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>

                    <div class="uk-margin">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                        <input id="password" type="password" class="uk-input @error('password') uk-form-danger @enderror" name="password" required autocomplete="new-password">
                        @error('password')
                        <span class="uk-alert-danger" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>

                    <div class="uk-margin">
                        <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>
                        <input id="password-confirm" type="password" class="uk-input" name="password_confirmation" required autocomplete="new-password">
                    </div>

                    <div class="uk-margin">
                        <button type="submit" class="uk-button uk-button-primary">
                            {{ __('Register') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
