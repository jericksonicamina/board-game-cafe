@extends('layouts.app')

@section('additional_css')
    <style>
        .login-container {
            display: flex;
            align-items: center;
            justify-content: center;
            padding: 40px 0;
            min-height: 72.1vh;
        }
        .login-wrapper {
            width: 500px;
        }
    </style>
@endsection

@section('content')
<div class="uk-container login-container">
    <div class="uk-child-width-1-2@s uk-grid-match">
        <div class="login-wrapper">
            <div class="uk-card uk-card-default uk-card-hover uk-card-body">
                <h3 class="uk-card-title">{{ __('Login') }}</h3>
                <form method="POST" class="uk-form-stacked" action="{{ route('login') }}">
                    @csrf
                    <div class="uk-margin">
                        <label for="email">{{ __('E-Mail Address') }}</label>
                        <input id="email" type="email" class="uk-input @error('email') uk-form-danger @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        @error('email')
                        <span class="uk-alert-danger" role="alert">
                            <a class="uk-alert-close" uk-close></a>
                            <p>{{ $message }}</p>
                        </span>
                        @enderror
                    </div>

                    <div class="uk-margin">
                        <label for="password">{{ __('Password') }}</label>
                        <input id="password" type="password" class="uk-input @error('password') uk-form-danger @enderror" name="password" required autocomplete="current-password">
                        @error('password')
                        <span class="uk-alert-danger" role="alert">
                                <a class="uk-alert-close" uk-close></a>
                                <p>{{ $message }}</p>
                            </span>
                        @enderror
                    </div>

                    <div class="uk-margin">
                        <input class="uk-checkbox" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                        <label class="form-check-label" for="remember">
                            {{ __('Remember Me') }}
                        </label>
                    </div>

                    <div class="uk-margin">
                        <div class="col-md-8 offset-md-4">
                            <button type="submit" class="uk-button uk-button-primary">
                                {{ __('Login') }}
                            </button>
                            <a href="{{ route('register') }}" class="uk-button uk-button-primary">Register</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
