@extends('layouts.app')

@section('additional_css')
    <link rel="stylesheet" href="{{ asset('plugins/datetimepicker/jquery.datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_dashboard.css') }}">
@endsection

@section('content')
    <div class="uk-container" id="admin-dashboard-container">
        <div>
            <a class="uk-button uk-button-primary" id="add-new-game-btn" href="#add-new-game-modal" uk-toggle>Add New Game</a>
            <a class="uk-button uk-button-primary" id="add-existing-game-btn" href="#add-existing-game-modal" uk-toggle>Add Existing Game</a>
        </div>
        <div id=games-data-section">
            <table class="uk-table uk-table-hover uk-table-striped uk-table-responsive display nowrap" id="games-data-table">
                <thead>
                <tr>
                    <th>Banner</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Player</th>
                    <th>Approx. Time</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <div id="add-new-game-modal" uk-modal bg-close="false">
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-header">
                <h4 class="uk-modal-title">Add new game</h4>
            </div>
            <div class="uk-modal-body">
                <form id="add-new-game-form" enctype="multipart/form-data">
                    <fieldset class="uk-fieldset">
                        <div class="uk-margin uk-grid" uk-grid>
                            <div class="uk-width-1-2@s">
                                <span class="uk-text-middle">Game Banner: </span>
                                <div uk-form-custom>
                                    <input type="file" name="game_banner" id="game-banner-file-input" required
                                           aria-required="true">
                                    <span class="uk-link">Choose Image</span>
                                </div>
                            </div>
                            <div class="uk-width-1-2@s">
                                <div id="add-new-game-banner-preview"></div>
                            </div>
                        </div>
                        <div class="uk-margin">
                            <input class="uk-input" name="game_name" type="text" placeholder="Name" required
                                   aria-required="true">
                        </div>
                        <div class="uk-margin">
                            <textarea class="uk-textarea" rows="5" name="game_description" placeholder="Description"
                                      required aria-required="true"></textarea>
                        </div>
                        <div class="uk-margin uk-grid" uk-grid>
                            <div class="uk-width-1-2@s">
                                <input class="uk-input" type="number" name="number_of_player_min" min="1"
                                       placeholder="Min number of player" required aria-required="true">
                            </div>
                            <div class="uk-width-1-2@s">
                                <input class="uk-input" type="number" name="number_of_player_max" min="1"
                                       placeholder="Max number of player" required aria-required="true">
                            </div>
                        </div>
                        <div class="uk-margin uk-grid" uk-grid>
                            <div class="uk-width-1-2@s">
                                <input class="uk-input" type="number" name="approx_play_time_min" min="1"
                                       placeholder="Min play time" required aria-required="true">
                            </div>
                            <div class="uk-width-1-2@s">
                                <input class="uk-input" type="number" name="approx_play_time_max" min="1"
                                       placeholder="Max play time" required aria-required="true">
                            </div>
                        </div>
                        <div class="uk-margin uk-grid" uk-grid>
                            <div class="uk-width-1-2@s">
                                <input class="uk-input" type="number" name="price" min="1" step="0.01"
                                       placeholder="Price" required aria-required="true">
                            </div>
                            <div class="uk-width-1-2@s">
                                <input class="uk-input" type="text" name="date_acquired" id="date-acquired" placeholder="Date Acquired"
                                       required aria-required="true">
                            </div>
                        </div>
                        <div class="uk-margin">
                            <button type="submit" class="uk-button uk-button-primary uk-align-right">Create</button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>

    <div id="add-existing-game-modal" uk-modal bg-close="false">
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-header">
                <h4 class="uk-modal-title">Add existing game</h4>
            </div>
            <div class="uk-modal-body">
                <form id="add-existing-game-form">
                    <div class="uk-margin">
                        <select id="existing-game-select" name="game_master_list_id" class="uk-select uk-input" required aria-required="true"></select>
                    </div>
                    <div class="uk-margin uk-grid" uk-grid>
                        <div class="uk-width-1-2@s">
                            <input class="uk-input" type="number" name="price" min="1" step="0.01" placeholder="Price" required aria-required="true">
                        </div>
                        <div class="uk-width-1-2@s">
                            <input class="uk-input" type="text" name="date_acquired" id="existing-date-acquired" placeholder="Date Acquired" required aria-required="true">
                        </div>
                    </div>
                    <div class="uk-margin">
                        <button type="submit" class="uk-button uk-button-primary uk-align-right">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('additional_js')
    <script src="{{ asset('plugins/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>
    <script src="{{ asset('js/admin_dashboard.js') }}"></script>
@endsection
