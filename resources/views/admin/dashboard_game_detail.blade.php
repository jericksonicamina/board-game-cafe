@extends('layouts.app')

@section('additional_css')
    <link rel="stylesheet" href="{{ asset('plugins/datetimepicker/jquery.datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/admin_dashboard.css') }}">
@endsection

@section('content')
    <div class="uk-container" id="admin-dashboard-container" data-game_master_list_id="{{ $game_master_list_id }}">
        <div id=games-data-section">
            <table class="uk-table uk-table-hover uk-table-striped uk-table-responsive display nowrap" id="games-data-table">
                <thead>
                <tr>
                    <th>Banner</th>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Status</th>
                    <th>Action</th>
                    <th>Description</th>
                    <th>Player</th>
                    <th>Approx. Time</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <div id="update-game-status-modal" uk-modal bg-close="false" data-game_id="0">
        <div class="uk-modal-dialog">
            <button class="uk-modal-close-default" type="button" uk-close></button>
            <div class="uk-modal-header">
                <h4 class="uk-modal-title">Update game status</h4>
            </div>
            <div class="uk-modal-body">
                <div class="uk-margin">
                    <label class="uk-form-label" for="game-status-select">Select New Status:</label>
                    <div class="uk-form-controls">
                        <select class="uk-select" id="game-status-select">
                            <option value="1">Available</option>
                            <option value="2">Borrowed</option>
                            <option value="3">Incomplete</option>
                            <option value="4">Lost</option>
                        </select>
                    </div>
                </div>
                <div class="uk-margin">
                    <button type="submit" class="uk-button uk-button-primary uk-align-right" id="update-game-status">Update</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('additional_js')
    <script src="{{ asset('plugins/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>
    <script src="{{ asset('js/admin_dashboard_game_details.js') }}"></script>
@endsection
