@extends('layouts.app')

@section('additional_css')
    <link rel="stylesheet" href="{{ asset('css/user_dashboard.css') }}">
@endsection

@section('content')
    <div class="uk-container" id="user-dashboard-container">
        <div id=games-data-section">
            <table class="uk-table uk-table-hover uk-table-striped uk-table-responsive display nowrap" id="games-data-table">
                <thead>
                <tr>
                    <th>Banner</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Player</th>
                    <th>Approx. Time</th>
                    <th>Action</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection

@section('additional_js')
    <script src="{{ asset('js/user_dashboard.js') }}"></script>
@endsection
