@extends('layouts.app')

@section('additional_css')
    <link rel="stylesheet" href="{{ asset('css/landing.css') }}">
@endsection

@section('content')
    <div class="book-criteria-container">
        <div class="uk-container">
            <h4 class="uk-text-lead">Find the game that you can enjoy together</h4>
            <form class="uk-grid-small" uk-grid id="landing-game-list-filter-form">
                <div class="uk-width-1-4@s">
                    <input class="uk-input" type="number" name="number_of_player_filter" min="1" placeholder="Min. No. of Players">
                </div>
                <div class="uk-width-1-4@s">
                    <input class="uk-input" type="number" name="time_duration_filter" min="1" placeholder="Min. Time Duration (minutes)">
                </div>
                <div class="uk-width-1-4@s">
                    <button class="uk-button uk-button-default" type="submit">Search</button>
                    <button class="uk-button uk-button-default" type="button" id="clear-filter">Clear Filter</button>
                </div>
            </form>
        </div>
    </div>
    <div class="uk-container available-games-container">
        <div class="pagination-container uk-flex uk-flex-right">
            <button class="uk-button-default uk-button-small" id="previous-page">Previous</button>
            <button class="uk-button-default uk-button-small" id="next-page">Next</button>
        </div>
        <div class="uk-grid-match uk-child-width-1-4@m" id="game-list-container" uk-grid></div>
    </div>
@endsection

@section('additional_js')
    <script src="{{ asset('js/landing.js') }}"></script>
@endsection
