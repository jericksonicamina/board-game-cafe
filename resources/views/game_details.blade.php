@extends('layouts.app')

@section('additional_css')
    <link rel="stylesheet" href="{{ asset('plugins/datetimepicker/jquery.datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/game_details.css') }}">
@endsection

@section('content')
    <div class="uk-container" id="main-container" data-game_master_list_id="{{ $game_master_list_id }}">
        <div class="uk-card uk-card-default">
            <div class="uk-card-media-top">
                <img src="" alt="" id="game-banner" width="350" class="uk-align-center">
            </div>
            <div class="uk-card-body">
                <h3 class="uk-card-title" id="game-name"></h3>
                <p id="game-description"></p>
                <p class="uk-text-meta"><b>Player: </b><span id="player-no"></span></p>
                <p class="uk-text-meta uk-margin-remove-top"><b>Approximate Time: </b><span id="approx-time"></span> minutes</p>
                <p class="uk-text-meta"><b>Available Count: </b><span id="available-count"></span></p>
                <a class="uk-button uk-button-primary" id="borrow-game" data-game_master_list_id="">Borrow Game</a>
            </div>
        </div>
    </div>
@endsection

@section('additional_js')
    <script src="{{ asset('plugins/datetimepicker/jquery.datetimepicker.full.min.js') }}"></script>
    <script src="{{ asset('js/game_details.js') }}"></script>
@endsection
