$(document).ready(function () {
    const $games_data_table = $('#games-data-table');
    const api_routes = {
        user_dashboard_game_by_id: '/game/user-dashboard/game-by-id',
        return_game: '/game/return-game',
    };


    /**
     * Games datatable
     */
    $games_data_table.DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: api_routes.user_dashboard_game_by_id,
            type: 'POST',
            beforeSend: function (e) {
                e.headers = {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr("content")}
            },
        },
        columns: [
            {
                data: 'game_banner',
                name: 'game_banner',
                searchable: false,
                sortable: false,
                render: function (id, type, full) {
                    const image_storage = full.game_banner.replace('public', '');
                    return `<img src="/storage${image_storage}" width="100" height="100" />`;
                }
            },
            {data: 'game_name', name: 'game_name'},
            {
                data: 'game_description',
                name: 'game_description',
                render: function (id, type, full) {
                    const description = (full.game_description.length <= 100) ? full.game_description : full.game_description.slice(0, 100) + '...';
                    return description;
                }
            },
            {
                data: 'number_of_player_min',
                name: 'number_of_players',
                render: function (id, type, full) {
                    return `<span>${full.number_of_player_min} - ${full.number_of_player_max} players</span>`;
                }
            },
            {
                data: 'approx_play_time_min',
                name: 'approx_play_time',
                render: function (id, type, full) {
                    console.log(full);
                    return `<span>${full.approx_play_time_min} - ${full.approx_play_time_max} minutes</span>`;
                }
            },
            {
                data: 'game_list_id',
                name: 'approx_play_time',
                render: function (id, type, full) {
                    return `<button data-game_id="${full.game_list_id}" class="uk-button uk-button-small uk-button-primary return-game">Return Game</button>`;
                }
            },
        ],
    });
    $(document).on('click', '.return-game', function () {
        const game_list_id = $(this).attr('data-game_id');
        $.post(api_routes.return_game, { game_list_id: game_list_id, status: 2}).done(function (response) {
            if (response.status !== apiResponseStatus.SUCCESS) {
                UIkit.notification(`<span uk-icon='icon: warning'></span> ${response.message}`, {
                    pos: 'bottom-right',
                    status: 'warning',
                    timeout: 5000
                });
                return true;
            }
            UIkit.notification(`<span uk-icon='icon: info'></span> Item successfully return`, {
                pos: 'bottom-right',
                status: 'success',
                timeout: 1000
            });
            setTimeout(function () {
                window.location.reload();
            }, 1100);
        });
    });
});
