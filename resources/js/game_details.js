$(document).ready(function () {
    const $main_container = $('#main-container');
    const game_master_list_id = $main_container.attr('data-game_master_list_id');
    const $borrow_game = $('#borrow-game');
    const api_routes = {
        admin_dashboard_game_master_list: '/game/admin-dashboard/game-master-list',
        game_details_by_id: '/game/details/by/id',
        update_game_master_list_status: '/game/update-game-master-list-status',
        borrow_game: '/game/borrow-game',
    };

    $.post(api_routes.game_details_by_id, {game_master_list_id: game_master_list_id}).done(function (response) {
        const game_details = response.payload.game_details;
        if (response.payload.item_count <= 0) {
            $borrow_game
                .removeClass('uk-button-primary')
                .addClass('uk-button-default')
                .addClass('uk-disabled').attr('disabled', true);
        }
        $('#game-banner').attr('src', '/' + game_details.game_banner.replace('public', 'storage'));
        $('#game-name').text(game_details.game_name);
        $('#game-description').text(game_details.game_description);
        $('#player-no').text(game_details.number_of_player_min + ' - ' + game_details.number_of_player_max);
        $('#available-count').text(response.payload.item_count);
        $('#approx-time').text(game_details.approx_play_time_min + ' - ' + game_details.approx_play_time_max);
        $('#borrow-game').attr('data-game_master_list_id', game_details.game_master_list_id);
    });

    $borrow_game.on('click', function () {
        const game_master_list_id = $(this).attr('data-game_master_list_id');
        $.post(api_routes.borrow_game, { game_master_list_id: game_master_list_id, status: 2}).done(function (response) {
            if (response.status !== apiResponseStatus.SUCCESS) {
                UIkit.notification(`<span uk-icon='icon: warning'></span> ${response.message}`, {
                    pos: 'bottom-right',
                    status: 'warning',
                    timeout: 5000
                });
                return true;
            }
            UIkit.notification(`<span uk-icon='icon: info'></span> Item successfully borrowed`, {
                pos: 'bottom-right',
                status: 'success',
                timeout: 1000
            });
            setTimeout(function () {
                window.location.reload();
            }, 1100);
        });
    });
});
