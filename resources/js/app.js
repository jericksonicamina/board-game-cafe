/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr("content"),
        'X-Requested-With': 'XMLHttpRequest'
    }
});

appHelper.convertSerializedArrayToOjbect = function (arr) {
    let return_object = {};
    arr.map(function (data) {
        return_object[data.name] = data.value;
    });
    return return_object;
};

/**
 * Navigation Search
 */
$(document).ready(function () {
    const all_games_search = '/game/all-games-search';
    const $global_game_list_search = $('#global-game-list-search');
    const $global_search_container = $('#global-search-container');
    $global_game_list_search.on('input', function () {
        const search_term = $(this).val();
        if (search_term.length < 3) {
            $global_search_container.css('display', 'none');
            return true;
        }
        $.post(all_games_search, {search: search_term}).done(function (response) {
            $global_search_container.css('display', 'block').find('ul').html(null);
            if (response.length <= 0) {
                $global_search_container.find('ul').append('<li>No search result found</li>');
                return true;
            }
            response.map(function (item) {
                let game_banner = item.game_banner.replace('public', 'storage');
                $global_search_container.find('ul').append(`<a href="/game-details/${item.game_master_list_id}"><li><img src="/${game_banner}" width="35" height="35"/> ${item.game_name}</li></a>`);
            });
        });
    });
    $(document).mouseup(function(e) {
        if (!$global_game_list_search.is(e.target) && $global_game_list_search.has(e.target).length === 0) {
            $global_search_container.css('display', 'none');
        }
    });
});
