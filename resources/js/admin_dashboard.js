$(document).ready(function () {
    const $game_banner_file_input = $('#game-banner-file-input');
    const $add_new_game_banner_preview = $('#add-new-game-banner-preview');
    const $add_new_game_form = $('#add-new-game-form');
    const $add_existing_game_form = $('#add-existing-game-form');
    const $games_data_table = $('#games-data-table');
    const $existing_game_select = $('#existing-game-select');
    const $date_acquired = $('#date-acquired');
    const $existing_date_acquired = $('#existing-date-acquired');
    const api_routes = {
        add_new_game: '/game/add',
        admin_dashboard_game_list: '/game/admin-dashboard/game-list',
        admin_dashboard_game_master_list: '/game/admin-dashboard/game-master-list',
        all_games_search: '/game/all-games-search',
        add_existing_game: '/game/add-existing-game',
        update_game_master_list_status: '/game/update-game-master-list-status',
    };

    $date_acquired.datetimepicker({format:'Y-m-d H:i:s'});
    $existing_date_acquired.datetimepicker({format:'Y-m-d H:i:s'});

    /**
     * Upload image preview
     */
    $game_banner_file_input.on('change', function () {
        $add_new_game_banner_preview.html(null);
        if (this.files && this.files[0]) {
            let reader = new FileReader();
            reader.onload = function(e) {
                $add_new_game_banner_preview.append(`<img src="${e.target.result}" />`);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    /**
     * Add game form submit
     */
    $add_new_game_form.on('submit', function (e) {
        e.preventDefault();
        let form_data = new FormData();
        form_data.append('game_banner', $game_banner_file_input[0].files[0]);
        form_data.append('csrfmiddlewaretoken', $('meta[name=csrf-token]').attr("content"));
        $(this).serializeArray().map(function (data) {
            // Skip the game banner file
            if (data.name === 'game_banner') {
                return true;
            }
            form_data.append(data.name, data.value);
        });

        $.ajax({
            url: api_routes.add_new_game,
            data: form_data,
            enctype: 'multipart/form-data',
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(response){
                UIkit.modal('#add-new-game-modal').hide();
                if (response.status !== apiResponseStatus.SUCCESS) {
                    UIkit.notification(`<span uk-icon='icon: warning'></span> ${response.message}`, {
                        pos: 'bottom-right',
                        status: 'warning',
                        timeout: 5000
                    });
                    return true;
                }
                UIkit.notification(`<span uk-icon='icon: info'></span> ${response.message}`, {
                    pos: 'bottom-right',
                    status: 'success',
                    timeout: 1000
                });
                setTimeout(function () {
                    window.location.reload();
                }, 1100);
            },
            error: function (error) {
                UIkit.modal('#add-new-game-modal').hide();
                UIkit.notification(`<span uk-icon='icon: close'></span> An error occurred, please contact the administrator`, {
                    pos: 'bottom-right',
                    status: 'warning',
                    timeout: 5000
                });
                console.log(error);
            }
        });
    });

    /**
     * Games datatable
     */
    $games_data_table.DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: api_routes.admin_dashboard_game_master_list,
            type: 'POST',
            beforeSend: function (e) {
                e.headers = {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr("content")}
            },
        },
        columns: [
            {
                data: 'game_banner',
                name: 'game_banner',
                searchable: false,
                sortable: false,
                render: function (id, type, full) {
                    const image_storage = full.game_banner.replace('public', '');
                    return `<img src="/storage${image_storage}" width="100" height="100" />`;
                }
            },
            {data: 'game_name', name: 'game_name'},
            {
                data: 'game_description',
                name: 'game_description',
                render: function (id, type, full) {
                    const description = (full.game_description.length <= 100) ? full.game_description : full.game_description.slice(0, 100) + '...';
                    return description;
                }
            },
            {
                data: 'number_of_player_min',
                name: 'number_of_players',
                render: function (id, type, full) {
                    return `<span>${full.number_of_player_min} - ${full.number_of_player_max} players</span>`;
                }
            },
            {
                data: 'approx_play_time_min',
                name: 'approx_play_time',
                render: function (id, type, full) {
                    return `<span>${full.approx_play_time_min} - ${full.approx_play_time_max} minutes</span>`;
                }
            },
            {
                data: 'game_master_list_id',
                name: 'approx_play_time',
                render: function (id, type, full) {
                    return `
                            <a href="/admin/dashboard/game-details/${full.game_master_list_id}" class="uk-button uk-button-small uk-button-primary" target="_blank">View</a>
                            <button data-game_id="${full.game_master_list_id}" class="uk-button uk-button-small uk-button-danger remove-game">Delete</button>
                            `;
                }
            },
        ],
    });

    /**
     * Add existing game
     */
    $existing_game_select.select2({
        placeholder: 'Search game name',
        minimumInputLength: 3,
        cache: true,
        ajax: {
            url: api_routes.all_games_search,
            dataType: 'json',
            type: 'POST',
            delay: 250,
            data: function (params) {
                return {
                    search: params.term
                };
            },
            processResults: function (response_data) {
                let display_data = [];
                response_data.map(function (data) {
                    display_data.push({
                        id: data.game_master_list_id,
                        text: data.game_name
                    });
                });
                return {
                    results: display_data
                }
            }
        }
    });
    $add_existing_game_form.on('submit', function (e) {
        e.preventDefault();
        let form_data = appHelper.convertSerializedArrayToOjbect($(this).serializeArray());
        $.post(api_routes.add_existing_game, { form_data: form_data}).done(function (response) {
            console.log(response);
            UIkit.modal('#add-existing-game-modal').hide();
            if (response.status !== apiResponseStatus.SUCCESS) {
                UIkit.notification(`<span uk-icon='icon: warning'></span> ${response.message}`, {
                    pos: 'bottom-right',
                    status: 'warning',
                    timeout: 5000
                });
                return true;
            }
            UIkit.notification(`<span uk-icon='icon: info'></span> ${response.message}`, {
                pos: 'bottom-right',
                status: 'success',
                timeout: 1000
            });

            // Reset input fields
            $add_existing_game_form.find('input').val(null);
            $existing_game_select.val(null).trigger('change');
        });
    });


    /**
     * Remove master list game item
     */
    $(document).on('click', '.remove-game', function () {
        const game_id = $(this).attr('data-game_id');
        $.post(api_routes.update_game_master_list_status, { game_master_list_id: game_id, status: 0}).done(function (response) {
            if (response.status !== apiResponseStatus.SUCCESS) {
                UIkit.notification(`<span uk-icon='icon: warning'></span> ${response.message}`, {
                    pos: 'bottom-right',
                    status: 'warning',
                    timeout: 5000
                });
                return true;
            }
            UIkit.notification(`<span uk-icon='icon: info'></span> Item successfully deleted`, {
                pos: 'bottom-right',
                status: 'success',
                timeout: 1000
            });
            setTimeout(function () {
                window.location.reload();
            }, 1100);
        });
    })
});
