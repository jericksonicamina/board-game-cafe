$(document).ready(function () {
    const $previous_page = $('#previous-page');
    const $next_page = $('#next-page');
    const $game_list_container = $('#game-list-container');
    const $landing_game_list_filter_form = $('#landing-game-list-filter-form');
    const $clear_filter = $('#clear-filter');
    let paginated_data = {};
    const api_routes = {
        admin_dashboard_game_master_list: '/game/admin-dashboard/game-master-list',
        paginated_game_list: '/game/paginated-game-list',
        set_game_list_filters: '/game/set-game-list-filters',
        clear_game_list_filters: '/game/clear-game-list-filters',
    };
    const renderTableDisplay = function(data, current_page, last_page) {
        $game_list_container.html(null);
        // Reset pagination button disable.
        $previous_page.removeClass('uk-disabled').attr('disabled', false);
        $next_page.removeClass('uk-disabled').attr('disabled', false);

        if (current_page === 1) {
            $previous_page.addClass('uk-disabled').attr('disabled', true);
        }
        if (current_page === last_page) {
            $next_page.addClass('uk-disabled').attr('disabled', true);
        }
        data.map(function (item) {
            $game_list_container.append(`<div>
                <div class="uk-card uk-card-default">
                    <div class="uk-card-media-top">
                        <img src="/${item.game_banner.replace('public', 'storage')}" alt="${item.game_name}">
                    </div>
                    <div class="uk-card-body">
                        <h3 class="uk-card-title">${item.game_name}</h3>
                        <p>${item.game_description}</p>
                        <p class="uk-text-meta"><b>Player: </b>${item.number_of_player_min} - ${item.number_of_player_max}</p>
                        <p class="uk-text-meta uk-margin-remove-top"><b>Approximate Time: </b>${item.approx_play_time_min} - ${item.approx_play_time_max} minutes</p>
                        <a class="uk-button uk-button-primary" href="/game-details/${item.game_master_list_id}">Borrow Game</a>
                    </div>
                </div>
            </div>`);
        });
    }

    /**
     * Fetch initial list of games.
     */
    $.post(api_routes.paginated_game_list, {}).done(function (response) {
        renderTableDisplay(response.payload.data, response.payload.current_page, response.payload.last_page);
        paginated_data = response.payload;
    });

    /**
     * Previous batch function
     */
    $previous_page.on('click', function () {
        let prev_page = paginated_data.prev_page_url;
        if (! paginated_data.prev_page_url) {
            prev_page = paginated_data.last_page_url;
        }
        $.post(prev_page, {}).done(function (response) {
            renderTableDisplay(response.payload.data, response.payload.current_page, response.payload.last_page);
            paginated_data = response.payload;
        });
    });

    /**
     * Next batch function
     */
    $next_page.on('click', function () {
        $.post(paginated_data.next_page_url, {}).done(function (response) {
            renderTableDisplay(response.payload.data, response.payload.current_page, response.payload.last_page);
            paginated_data = response.payload;
        });
    });

    /**
     * Home game list filter
     */
    $landing_game_list_filter_form.on('submit', function (e) {
        e.preventDefault();
        let form_data = appHelper.convertSerializedArrayToOjbect($(this).serializeArray());
        console.log(form_data);
        $.post(api_routes.set_game_list_filters, form_data).done(function (response) {
            window.location.reload();
        });
    });

    /**
     * Clear filters
     */
    $clear_filter.on('click', function () {
        $.post(api_routes.clear_game_list_filters, {}).done(function () {
            window.location.reload();
        });
    });

    /**
     * Borrow game function
     */
    $(document).on('click', '.borrow-game-btn', function () {
        const game_master_list_id = $(this).attr('data-game_master_list_id');
    });
});
