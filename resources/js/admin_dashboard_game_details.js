$(document).ready(function () {
    const $games_data_table = $('#games-data-table');
    const api_routes = {
        admin_dashboard_game_list: '/game/admin-dashboard/game-list',
        admin_dashboard_game_by_id: '/game/admin-dashboard/game-by-id',
        update_game_status: '/game/update-game-status',
    };
    $games_data_table.DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ajax: {
            url: api_routes.admin_dashboard_game_by_id,
            data: {
                game_master_list_id: $('#admin-dashboard-container').attr('data-game_master_list_id')
            },
            type: 'POST',
            beforeSend: function (e) {
                e.headers = {'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr("content")}
            },
        },
        columns: [
            {
                data: 'game_banner',
                name: 'game_banner',
                searchable: false,
                sortable: false,
                render: function (id, type, full) {
                    const image_storage = full.game_banner.replace('public', '');
                    return `<img src="/storage${image_storage}" width="100" height="100" />`;
                }
            },
            {data: 'game_name', name: 'game_name'},
            {
                data: 'price',
                name: 'price',
                render: function (id, type, full) {
                    return `<span>$ ${full.price}</span>`;
                }
            },
            {
                data: 'status',
                name: 'status',
                render: function (id, type, full) {
                    /**
                     * Status guide:
                     * 1 = Available, 2 = Borrowed, 3 = Incomplete, 4 = Lost
                     */
                    let text_status = '';
                    switch (full.status) {
                        case 1:
                            text_status = 'Available'
                            break;
                        case 2:
                            text_status = 'Borrowed'
                            break;
                        case 3:
                            text_status = 'Incomplete'
                            break;
                        case 4:
                            text_status = 'Lost'
                            break;
                    }
                    return text_status;
                }
            },
            {
                data: 'game_list_id',
                name: 'approx_play_time',
                searchable: false,
                sortable: false,
                render: function (id, type, full) {
                    return `
                            <button data-game_id="${full.game_list_id}" data-game_status="${full.status}" class="uk-button uk-button-small uk-button-primary update-game-status">Update Status</button>
                            <button data-game_id="${full.game_list_id}" class="uk-button uk-button-small uk-button-danger remove-game">Delete</button>
                            `;
                }
            },
            {
                data: 'game_description',
                name: 'game_description',
                render: function (id, type, full) {
                    const description = (full.game_description.length <= 100) ? full.game_description : full.game_description.slice(0, 100) + '...';
                    return description;
                }
            },
            {
                data: 'number_of_player_min',
                name: 'number_of_players',
                render: function (id, type, full) {
                    return `<span>${full.number_of_player_min} - ${full.number_of_player_max} players</span>`;
                }
            },
            {
                data: 'approx_play_time_min',
                name: 'approx_play_time',
                render: function (id, type, full) {
                    return `<span>${full.approx_play_time_min} - ${full.approx_play_time_max} minutes</span>`;
                }
            },
        ],
    });

    /**
     * Show modal for status update
     */
    $(document).on('click', '.update-game-status', function () {
        const game_id = $(this).attr('data-game_id');
        const game_status = $(this).attr('data-game_status');
        $('#update-game-status-modal').attr('data-game_id', game_id);
        UIkit.modal('#update-game-status-modal').show().then(function () {
            $('#game-status-select').val(game_status);
        });
    });
    /**
     * Actual update status
     */
    $('#update-game-status').on('click', function () {
        const game_id = $('#update-game-status-modal').attr('data-game_id');
        const status = $('#game-status-select').val();
        $.post(api_routes.update_game_status, { game_list_id: game_id, status: status}).done(function (response) {
            UIkit.modal('#update-game-status-modal').hide();
            if (response.status !== apiResponseStatus.SUCCESS) {
                UIkit.notification(`<span uk-icon='icon: warning'></span> ${response.message}`, {
                    pos: 'bottom-right',
                    status: 'warning',
                    timeout: 5000
                });
                return true;
            }
            UIkit.notification(`<span uk-icon='icon: info'></span> ${response.message}`, {
                pos: 'bottom-right',
                status: 'success',
                timeout: 1000
            });
            setTimeout(function () {
                window.location.reload();
            }, 1100);
        });
    });

    /**
     * Remove game item
     */
    $(document).on('click', '.remove-game', function () {
        const game_id = $(this).attr('data-game_id');
        $.post(api_routes.update_game_status, { game_list_id: game_id, status: 0}).done(function (response) {
            if (response.status !== apiResponseStatus.SUCCESS) {
                UIkit.notification(`<span uk-icon='icon: warning'></span> Item successfully deleted`, {
                    pos: 'bottom-right',
                    status: 'warning',
                    timeout: 5000
                });
                return true;
            }
            UIkit.notification(`<span uk-icon='icon: info'></span> ${response.message}`, {
                pos: 'bottom-right',
                status: 'success',
                timeout: 1000
            });
            setTimeout(function () {
                window.location.reload();
            }, 1100);
        });
    });

});
