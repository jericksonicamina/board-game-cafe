<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Administrator',
            'email' => 'admin@boardgamecafe.com',
            'level' => 2,
            'password' => Hash::make('pass1234'),
        ]);
    }
}
