<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GameMasterlist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_master_list', function (Blueprint $table) {
            $table->id('game_master_list_id');
            $table->char('game_name');
            $table->longText('game_description');
            $table->char('game_banner');
            $table->integer('number_of_player_min');
            $table->integer('number_of_player_max');
            $table->integer('approx_play_time_min');
            $table->integer('approx_play_time_max');
            $table->integer('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_master_list');
    }
}
