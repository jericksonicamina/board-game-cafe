<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GameList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('game_list', function (Blueprint $table) {
            $table->id('game_list_id');
            $table->foreignId('game_master_list_id');
            $table->foreignId('user_id');
            $table->float('price');
            $table->dateTime('date_acquired');
            $table->foreignId('borrowed_by')->default(0);
            $table->integer('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('game_list');
    }
}
