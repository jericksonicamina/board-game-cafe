<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameModel extends Model
{
    protected $table = 'game_master_list';
    protected $primaryKey = 'game_master_list_id';
}
