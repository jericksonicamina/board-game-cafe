<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GameItemModel extends Model
{
    protected $table = 'game_list';
    protected $primaryKey = 'game_list_id';
}
