<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const API_SUCCESS = 'Success';
    const API_FAILED = 'Failed';
    const API_ERROR = 'Error';

    public function apiResponse($type, $message, $data = [])
    {
        /*
         * The type ('Success', 'Failed', 'Error') and message parameters must be string
         * while data must be an array
         */
        $response = [
            'status' => $type,
            'message' => 'Request ' . $type . ': ' . $message,
        ];

        if ($type == 'Success') {
            $response['payload'] = $data;
        }
        return response()->json($response);
    }
}
