<?php

namespace App\Http\Controllers;

use App\GameItemModel;
use App\GameModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class GameController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addNewGame(Request $request)
    {
        // Sanitize the data input for another layer of protection.
        $game_name = filter_var($request->input('game_name'), FILTER_SANITIZE_STRING);
        $game_description = filter_var($request->input('game_description'), FILTER_SANITIZE_STRING);
        $number_of_player_min = filter_var($request->input('number_of_player_min'), FILTER_SANITIZE_NUMBER_INT);
        $number_of_player_max = filter_var($request->input('number_of_player_max'), FILTER_SANITIZE_NUMBER_INT);
        $approx_play_time_min = filter_var($request->input('approx_play_time_min'), FILTER_SANITIZE_NUMBER_INT);
        $approx_play_time_max = filter_var($request->input('approx_play_time_max'), FILTER_SANITIZE_NUMBER_INT);
        $price = filter_var($request->input('price'), FILTER_SANITIZE_NUMBER_FLOAT);
        // Check the game name if it exist before creating new data.
        $game_exist = DB::table('game_master_list')->where('game_name', $game_name)->exists();
        if ($game_exist) {
            return $this->apiResponse(self::API_FAILED, 'Game already existing');
        }
        // Create new game
        // Upload the file
        $path = $request->file('game_banner')->store('public');
        $game_model = new GameModel();
        $game_model->game_name = $game_name;
        $game_model->game_description = $game_description;
        $game_model->game_banner = $path;
        $game_model->number_of_player_min = $number_of_player_min;
        $game_model->number_of_player_max = $number_of_player_max;
        $game_model->approx_play_time_min = $approx_play_time_min;
        $game_model->approx_play_time_max = $approx_play_time_max;
        $game_model->status = 1;
        $game_model->save();

        // Create initial game item.
        $game_item = new GameItemModel();
        $game_item->game_master_list_id = $game_model->game_master_list_id;
        $game_item->user_id = auth()->user()->id;
        $game_item->price = $price;
        $game_item->date_acquired = $request->input('date_acquired');
        $game_item->status = 1;
        $game_item->save();
        return $this->apiResponse(self::API_SUCCESS, 'New game created', [$game_model->game_master_list_id, $game_item->game_list_id]);
    }

    public function getGameMasterList(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('game_master_list')->where('status', 1)->get();
            return DataTables::of($data)->make(true);
        }
    }

    public function adminDashboardGameList(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('game_list')
                ->leftJoin('game_master_list', 'game_list.game_master_list_id', '=', 'game_master_list.game_master_list_id')
                ->select('game_list.game_list_id', 'game_list.price', 'game_list.date_acquired', 'game_list.status', 'game_master_list.game_name',
                    'game_master_list.game_description', 'game_master_list.game_banner',
                    'game_master_list.number_of_player_min', 'game_master_list.number_of_player_max',
                    'game_master_list.approx_play_time_min', 'game_master_list.approx_play_time_max')
                ->where('game_master_list.status', '!=', 0)
                ->where('game_list.status', '!=', 0)
                ->get();
            return DataTables::of($data)->make(true);
        }
    }

    public function adminDashboardGameById(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('game_list')
                ->leftJoin('game_master_list', 'game_list.game_master_list_id', '=', 'game_master_list.game_master_list_id')
                ->select('game_list.game_list_id', 'game_list.price', 'game_list.date_acquired', 'game_list.status', 'game_master_list.game_name',
                    'game_master_list.game_description', 'game_master_list.game_banner',
                    'game_master_list.number_of_player_min', 'game_master_list.number_of_player_max',
                    'game_master_list.approx_play_time_min', 'game_master_list.approx_play_time_max')
                ->where('game_master_list.status', '!=', 0)
                ->where('game_list.status', '!=', 0)
                ->where('game_master_list.game_master_list_id', $request->input('game_master_list_id'))
                ->get();
            return DataTables::of($data)->make(true);
        }
    }

    public function addExistingGame(Request $request)
    {
        $form_data = $request->input('form_data');
        $game_item = new GameItemModel();
        $game_item->game_master_list_id = $form_data['game_master_list_id'];
        $game_item->user_id = auth()->user()->id;
        $game_item->price = $form_data['price'];
        $game_item->date_acquired = $form_data['date_acquired'];
        $game_item->status = 1;
        $game_item->save();
        return $this->apiResponse(self::API_SUCCESS, 'New game created', $game_item->game_list_id);
    }

    public function allGamesSearch(Request $request)
    {
        $search_term = $request->input('search');
        $game_data = DB::table('game_master_list')
            ->where('game_name', 'LIKE', '%' . $search_term . '%')->take(20)->get();
        return response()->json($game_data);
    }

    public function updateGameStatus(Request $request)
    {
        $game_list_id = $request->input('game_list_id');
        $status = $request->input('status');
        $affected = DB::table('game_list')
            ->where('game_list_id', $game_list_id)
            ->update(['status' => $status]);
        return $this->apiResponse(self::API_SUCCESS, 'Game updated.', $affected);
    }

    public function updateGameMasterListStatus(Request $request)
    {
        $game_master_list_id = $request->input('game_master_list_id');
        $status = $request->input('status');
        $affected = DB::table('game_master_list')
            ->where('game_master_list_id', $game_master_list_id)
            ->update(['status' => $status]);
        return $this->apiResponse(self::API_SUCCESS, 'Game updated.', $affected);
    }

    public function getPaginatedGameList(Request $request)
    {
        $number_of_player_filter = $request->cookie('number_of_player_filter') ? $request->cookie('number_of_player_filter') : "";
        $time_duration_filter = $request->cookie('time_duration_filter') ? $request->cookie('time_duration_filter') : "";
        $game_data = DB::table('game_master_list')
            ->where('number_of_player_min', '>=', $number_of_player_filter)
            ->where('approx_play_time_min', '>=', $time_duration_filter)
            ->where('status', 1)
            ->paginate(8);
        return $this->apiResponse(self::API_SUCCESS, 'Game list.', $game_data);
    }

    public function setGameListFilter(Request $request)
    {
        $number_of_player = $request->input('number_of_player_filter');
        $time_duration = $request->input('time_duration_filter');
        Cookie::queue(Cookie::make('number_of_player_filter', $number_of_player, 360));
        Cookie::queue(Cookie::make('time_duration_filter', $time_duration, 360));
        return $this->apiResponse(self::API_SUCCESS, 'Setting filter.', [$request->cookie('number_of_player_filter'), $request->cookie('time_duration_filter')]);
    }

    public function removeGameListFilter()
    {
        Cookie::queue(Cookie::forget('number_of_player_filter'));
        Cookie::queue(Cookie::forget('time_duration_filter'));
        return $this->apiResponse(self::API_SUCCESS, 'Clearing filters.');
    }

    public function gameDetailsById(Request $request)
    {
        if ($request->ajax()) {
            $game_master_list_id = $request->input('game_master_list_id');
            $item_count = DB::table('game_list')->where('game_master_list_id', $game_master_list_id)->where('status', 1)->count();
            $data = DB::table('game_master_list')->where('game_master_list_id', $game_master_list_id)->first();
            return $this->apiResponse(self::API_SUCCESS, 'Game details by id.', ['game_details' => $data, 'item_count' => $item_count]);
        }
    }

    public function borrowGame(Request $request)
    {
        $game_master_list_id = $request->input('game_master_list_id');
        $game_list_id = DB::table('game_list')->select('game_list_id')->where('status', 1)->where('game_master_list_id', $game_master_list_id)->first();
        $affected = DB::table('game_list')
            ->where('game_list_id', $game_list_id->game_list_id)
            ->update(['status' => 2, 'borrowed_by' => auth()->user()->id]);
        return $this->apiResponse(self::API_SUCCESS, 'Game borrowed.', $affected);
    }

    public function returnGame(Request $request)
    {
        $game_list_id = $request->input('game_list_id');
        $affected = DB::table('game_list')
            ->where('game_list_id', $game_list_id)
            ->update(['status' => 1, 'borrowed_by' => 0]);
        return $this->apiResponse(self::API_SUCCESS, 'Game borrowed.', $affected);
    }

    public function userDashboardGameById(Request $request)
    {
        if ($request->ajax()) {
            $data = DB::table('game_list')
                ->leftJoin('game_master_list', 'game_list.game_master_list_id', '=', 'game_master_list.game_master_list_id')
                ->select('game_list.game_list_id', 'game_list.price', 'game_list.date_acquired', 'game_list.status', 'game_master_list.game_name',
                    'game_master_list.game_description', 'game_master_list.game_banner',
                    'game_master_list.number_of_player_min', 'game_master_list.number_of_player_max',
                    'game_master_list.approx_play_time_min', 'game_master_list.approx_play_time_max')
                ->where('game_list.borrowed_by', auth()->user()->id)
                ->get();
            return DataTables::of($data)->make(true);
        }
    }
}
