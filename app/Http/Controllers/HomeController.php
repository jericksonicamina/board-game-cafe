<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function loginCheck()
    {
        if (auth()->user()->level == 2) {
            return redirect()->route('admin_dashboard');
        } else {
            return redirect()->route('user_dashboard');
        }
    }

    public function adminDashboard()
    {
        return view('admin.dashboard');
    }

    public function adminDashboardGameDetails($game_master_list_id)
    {
        return view('admin.dashboard_game_detail')->with('game_master_list_id', $game_master_list_id);
    }

    public function dashboard()
    {
        return view('user.dashboard');
    }

    public function gameDetails($game_master_list_id)
    {
        return view('game_details')->with('game_master_list_id', $game_master_list_id);
    }
}
