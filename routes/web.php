<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login-check', 'HomeController@loginCheck')->name('login_check');
Route::get('/game-details/{game_master_list_id?}', 'HomeController@gameDetails')->name('game_details');
Route::middleware('auth')->prefix('admin')->group(function() {
    Route::get('/dashboard', 'HomeController@adminDashboard')->name('admin_dashboard')->middleware('is.admin');
    Route::get('/dashboard/game-details/{game_master_list_id?}', 'HomeController@adminDashboardGameDetails')->name('admin_dashboard_game_details')->middleware('is.admin');
});
Route::prefix('game')->group(function() {
    Route::any('/add', 'GameController@addNewGame')->name('add_new_game')->middleware('auth');
    Route::any('/admin-dashboard/game-list', 'GameController@adminDashboardGameList')->name('admin_dashboard_game_list')->middleware('auth');
    Route::any('/admin-dashboard/game-by-id', 'GameController@adminDashboardGameById')->name('admin_dashboard_game_by_id')->middleware('auth');
    Route::post('/user-dashboard/game-by-id', 'GameController@userDashboardGameById')->name('user_dashboard_game_by_id')->middleware('auth');
    Route::any('/admin-dashboard/game-master-list', 'GameController@getGameMasterList')->name('get_game_master_list');
    Route::any('/all-games-search', 'GameController@allGamesSearch')->name('all_games_search');
    Route::any('/add-existing-game', 'GameController@addExistingGame')->name('add_existing_game');
    Route::any('/update-game-status', 'GameController@updateGameStatus')->name('update_game_status')->middleware('auth');
    Route::any('/update-game-master-list-status', 'GameController@updateGameMasterListStatus')->name('update_game_master_list_status')->middleware('auth');
    Route::any('/paginated-game-list', 'GameController@getPaginatedGameList')->name('paginated_game_list');
    Route::any('/set-game-list-filters', 'GameController@setGameListFilter')->name('set_game_list_filters');
    Route::any('/clear-game-list-filters', 'GameController@removeGameListFilter')->name('clear_game_list_filters');
    Route::post('/details/by/id', 'GameController@gameDetailsById')->name('game_details_by_id');
    Route::post('/borrow-game', 'GameController@borrowGame')->name('borrow_game');
    Route::post('/return-game', 'GameController@returnGame')->name('return_game');
});
Route::middleware('auth')->prefix('user')->group(function() {
    Route::get('/dashboard', 'HomeController@dashboard')->name('user_dashboard');
});
